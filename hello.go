package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"bytes"
)

type keyboardButton struct {
	Text string `json:"text"`
}

type KeyboardButton interface {}

type replyKeyboard struct {
	Keyboard [][]KeyboardButton `json:"keyboard"`
	Selective string `json:"selective,string"`
}

type ReplyKeyboard interface {}

func NewReplyKeyboard(kb [][]KeyboardButton) ReplyKeyboard {
	return replyKeyboard{kb, "true"}
}

func main() {
	reply := NewReplyKeyboard([][]KeyboardButton{{keyboardButton{Text:"test"}}})
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		data, _ := json.Marshal(reply)
		_, e := writer.Write(data)
		if e != nil {
			fmt.Println(e)
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(request.Body)
		fmt.Println(buf.String())
	})
	err := http.ListenAndServeTLS(":443", "/etc/letsencrypt/live/faradenza.ml/fullchain.pem", "/etc/letsencrypt/live/faradenza.ml/privkey.pem", nil)
	if err != nil {
		fmt.Println(err)
	}
}

